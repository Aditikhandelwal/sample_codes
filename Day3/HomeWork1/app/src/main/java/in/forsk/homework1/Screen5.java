package in.forsk.homework1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Screen5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen5);

         /*To hide ActionBar*/
        getSupportActionBar().hide();
    }
}
